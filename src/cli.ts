import { ArgumentParser } from "argparse";
import { readFileSync } from "fs";
import { safeLoad } from "js-yaml";

import { Schemas, validateSchema } from "./schema";

const parser = new ArgumentParser({
  prog: "Validate Plex Schema",
  version: process.env.npm_package_version,
  addHelp: true,
  description: ""
});

parser.addArgument(["-t", "--type"], {
  required: true,
  choices: Object.values(Schemas),
  help: "Validate type"
});

parser.addArgument(["-f", "--format"], {
  choices: ["fast", "full"],
  help: "Format type"
});

parser.addArgument(["--first-error"], {
  action: "storeTrue",
  help: "Stop at first error"
});

parser.addArgument("files", {
  nargs: "+",
  help: "File to be validate"
});

const args = parser.parseArgs();
const { type, files, format, first_error: firstError } = args;
const jsonRegex = /\.json$/i;
const yamlRegex = /\.ya?ml$/i;
const options = { format, allErrors: firstError !== true };

files.forEach((file: string) => {
  const content = readFileSync(file, "utf8");
  let value = null;
  if (jsonRegex.test(file)) {
    value = JSON.parse(content);
  } else if (yamlRegex.test(file)) {
    value = safeLoad(content);
  }

  if (value === null) {
    console.warn(`Skipping ${file} before of unknown format`);
    return;
  }

  const result = validateSchema(type, value, options);
  if (!result.valid) {
    console.error(`There are error(s) in ${file}.`);
    console.dir(result.errors, { depth: null });
  }
});
