export * from "./schema";
export * from "./format";

if (require.main === module) {
  require("./cli");
}
