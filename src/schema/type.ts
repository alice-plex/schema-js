export type Actor = {
  name: string;
  role: string;
  photo: string | null;
};

export type Album = {
  aired: string;
  summary: string;
  genres: string[];
  collections: string[];
};

export type Artist = {
  similar: string[];
  summary: string;
  genres: string[];
  collections: string[];
};

export type Episode = {
  title: string[];
  content_rating: string;
  aired: string;
  summary: string;
  rating: number | null;
  directors: string[];
  writers: string[];
};

export type Movie = {
  title: string;
  sort_title: string;
  original_title: string[];
  content_rating: string;
  tagline: string[];
  studio: string[];
  aired: string;
  summary: string;
  rating: number | null;
  genres: string[];
  collections: string[];
  actors: Actor[];
  directors: string[];
  writers: string[];
};

export type Show = {
  title: string;
  sort_title: string;
  original_title: string[];
  content_rating: string;
  tagline: string[];
  studio: string[];
  aired: string;
  summary: string;
  rating: number | null;
  genres: string[];
  collections: string[];
  actors: Actor[];
  season_summary: { [key: string]: string };
};
