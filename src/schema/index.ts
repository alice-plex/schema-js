import Ajv, { ErrorObject, ValidateFunction } from "ajv";

import { Album, Artist, Movie, Show } from "./type";
import defsSchema from "./defs.schema.json";
import actorSchema from "./actor.schema.json";
import showSchema from "./show.schema.json";
import movieSchema from "./movie.schema.json";
import albumSchema from "./album.schema.json";
import artistSchema from "./artist.schema.json";
import { patchIRI } from "./patch";

export * from "./type";
export enum Schemas {
  Show = "show",
  Movie = "movie",
  Album = "album",
  Artist = "artist"
}

const ajvCache: { [key: string]: Ajv.Ajv } = {};

const createAjv = (options: Ajv.Options): Ajv.Ajv => {
  const key = JSON.stringify(options);
  if (key in ajvCache) {
    return ajvCache[key];
  }
  const ajv = patchIRI(new Ajv(options));
  ajv.addSchema(defsSchema);
  ajv.addSchema(actorSchema);
  ajv.addSchema(showSchema, Schemas.Show);
  ajv.addSchema(movieSchema, Schemas.Movie);
  ajv.addSchema(albumSchema, Schemas.Album);
  ajv.addSchema(artistSchema, Schemas.Artist);
  ajvCache[key] = ajv;
  return ajv;
};

export type Schema = ValidateFunction;
export type ValidationResult<T> =
  | { valid: true; data: T }
  | { valid: false; errors: ErrorObject[] };
export type ValidateSchemaOptions = {
  allErrors?: boolean;
  format?: "fast" | "full";
};

export const validateSchema: {
  (
    schema: Schemas.Show,
    data: any,
    options?: ValidateSchemaOptions
  ): ValidationResult<Show>;
  (
    schema: Schemas.Movie,
    data: any,
    options?: ValidateSchemaOptions
  ): ValidationResult<Movie>;
  (
    schema: Schemas.Album,
    data: any,
    options?: ValidateSchemaOptions
  ): ValidationResult<Album>;
  (
    schema: Schemas.Artist,
    data: any,
    options?: ValidateSchemaOptions
  ): ValidationResult<Artist>;
} = (
  schema: Schemas,
  data: any,
  options: ValidateSchemaOptions = {}
): ValidationResult<any> => {
  const { allErrors = false, format = "full" } = options;
  const ajv = createAjv({ allErrors, format });

  const validate = ajv.getSchema(schema);
  const valid = validate(data);
  if (valid) {
    return { valid: true, data };
  }
  const errors = validate.errors || [];
  return { valid: false, errors };
};
