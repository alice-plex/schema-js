# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [2.0.2]

### Added

- Added `normalizeTitle`

## [2.0.1]

### Changed

- Removed `@babel/polyfill`

## [2.0.0]

### Added

- Added documentations
- Added formatting functions

### Changed

- Migrated to webpack

## [1.1.0]

### Added

- Work around for #1

### Changed

- Update CLI help.
- Default format changed to "full"

## 1.0.0

### Added

- Initial Release.

[unreleased]: https://gitlab.com/alice-plex/schema-js/compare/2.0.2...master
[2.0.2]: https://gitlab.com/alice-plex/schema-js/compare/2.0.1...2.0.2
[2.0.1]: https://gitlab.com/alice-plex/schema-js/compare/2.0.0...2.0.1
[2.0.0]: https://gitlab.com/alice-plex/schema-js/compare/1.1.0...2.0.0
[1.1.0]: https://gitlab.com/alice-plex/schema-js/compare/1.0.0...1.1.0
