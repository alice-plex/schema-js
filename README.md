# @aliceplex/schema

JavaScript package to validate Alice Plex Schema.

For full documentation, check out [here](https://aliceplex-schemajs.joshuaavalon.app).

## Install

```
npm install @aliceplex/schema
```

## Usage

### JSON Schema

Current the schemas are written in JSON Schema Draft 7.

They can be found in [src/lib/schema](src/lib/schema). They can also be accessed through the url in `$id`.

- [Show](https://aliceplex-schemajs.joshuaavalon.app/show.schema.json)
- [Movie](https://aliceplex-schemajs.joshuaavalon.app/movie.schema.json)
- [Album](https://aliceplex-schemajs.joshuaavalon.app/album.schema.json)
- [Artist](https://aliceplex-schemajs.joshuaavalon.app/artist.schema.json)

### CLI

```
> vps -h

usage: Validate Plex Schema [-h] [-v] -t {show,movie,album,artist}
                            [-f {fast,full}] [--first-error]
                            files [files ...]

Positional arguments:
  files                 File to be validate

Optional arguments:
  -h, --help            Show this help message and exit.
  -v, --version         Show program's version number and exit.
  -t {show,movie,album,artist}, --type {show,movie,album,artist}
                        Validate type
  -f {fast,full}, --format {fast,full}
                        Format type
  --first-error         Stop at first error
```

### JavaScript Package

```typescript
import { Schemas, validateSchema } from "@aliceplex/schema";

const yaml = loadYaml("show.yaml"); // Implementation loadYaml yourself
const result = validateSchema(Schemas.Show, yaml);

if (result.valid) {
  console.log(result.data);
} else {
  console.log(result.errors);
}
```
