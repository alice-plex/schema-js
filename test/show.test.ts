import { Schemas, validateSchema } from "../src";

import { loadYaml } from "./utils";

test("Test valid show YAML", () => {
  const yaml = loadYaml("show.yaml");
  const result = validateSchema(Schemas.Show, yaml);
  if (!result.valid) {
    console.error(result.errors);
  }
  expect(result.valid).toBe(true);
  if (!result.valid) {
    return;
  }
  expect(result.data).toBeDefined();
});

test("Test show YAML missing title", () => {
  const yaml = loadYaml("show.e1.yaml");
  const result = validateSchema(Schemas.Show, yaml);
  if (result.valid) {
    console.error(result.data);
  }
  expect(result.valid).toBe(false);
  if (result.valid) {
    return;
  }
  expect(result.errors).toBeDefined();
  expect(result.errors).toHaveLength(1);
});

test("Test show YAML missing rating", () => {
  const yaml = loadYaml("show.c1.yaml");
  const result = validateSchema(Schemas.Show, yaml);
  if (!result.valid) {
    console.error(result.errors);
  }
  expect(result.valid).toBe(true);
  if (!result.valid) {
    return;
  }
  expect(result.data).toBeDefined();
});
