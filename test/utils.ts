import { readFileSync } from "fs";
import { safeLoad } from "js-yaml";
import * as path from "path";

const resolveDataPath = (...relativePaths: string[]): string =>
  path.resolve(__dirname, "data", ...relativePaths);

export const loadYaml = (file: string): any =>
  safeLoad(readFileSync(resolveDataPath(file), "utf8"));
