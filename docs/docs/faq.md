# Frequently Asked Questions

## What version of JavaScript does it support?

It is compiled to ES6 from TypeScript. It is usable in both web and node.

By default, tests are done on Node 10.

## Does it support TypeScript?

Yes, it is written in TypeScript and shipped with TypeScript definitions.

## How to build from source?

```
npm run build:script
npm run build
```

You can reference `.gitlab-ci.yml`.

- `build:script` builds utility scripts.
- `build` builds source code and tests.

## How do I use JSON Schemas?

You can check out the [libraries](https://json-schema.org/implementations.html) that support Draft-07.
