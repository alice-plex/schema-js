# Advance Usage

## validateSchema

### Definition

```typescript
const validateSchema: {
  (
    schema: Schemas.Show,
    data: any,
    options?: ValidateSchemaOptions
  ): ValidationResult<Show>;
  (
    schema: Schemas.Movie,
    data: any,
    options?: ValidateSchemaOptions
  ): ValidationResult<Movie>;
  (
    schema: Schemas.Album,
    data: any,
    options?: ValidateSchemaOptions
  ): ValidationResult<Album>;
  (
    schema: Schemas.Artist,
    data: any,
    options?: ValidateSchemaOptions
  ): ValidationResult<Artist>;
};
```

- `schema` is the schema used for validation.
- `data` is the object that being validated.
- `options` are optional options for validation.

  - `allErrors` return all errors or stop at first error. Default is `true`.
  - `format` determine is the content valid. It can be `fast` or `full`. Default is `full`.

    For example, `25:00` is not a valid time but it passes in `fast`.
